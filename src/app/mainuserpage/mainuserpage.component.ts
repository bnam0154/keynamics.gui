import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-mainuserpage',
  templateUrl: './mainuserpage.component.html',
  styleUrls: ['./mainuserpage.component.scss']
})
export class MainuserpageComponent implements OnInit {

  // cors = "https://cors-anywhere.herokuapp.com/"
  // baseURL = this.cors + "https://keynamics-core.herokuapp.com/";
  baseURL = "http://localhost:5000/"

  user_id = '';
  userName = '';
  score = '';
  log_prob = '';
  prob = '';
  keystroke_data = null;

  constructor(
    private http: HttpClient,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.keystroke_data = new Blob([params.keystroke_data, '|', JSON.stringify(params.predicted_id)], {type: 'text/plain'});
      // this.keystroke_data = new Blob([params.keystroke_data, '|', JSON.stringify('9')], {type: 'text/plain'});
      this.user_id = params.predicted_id;
      this.userName = params.userName;
      this.score = params.score;
      this.log_prob = params.log_prob;
      this.prob = params.prob;
    });
  }

  postLoginDataToServer (data): Observable<any> {
    return this.http.post(this.baseURL + 'loginfeedback', data)
  }

  postUntrainedDataToServer (data): Observable<any> {
    return this.http.post(this.baseURL + 'addtountraineddata', data)
  }

  correctUser() {
    
    this.postLoginDataToServer([1,this.user_id]).subscribe(
      res => {}
    );

    this.postUntrainedDataToServer(this.keystroke_data).subscribe(
      res => {}
    );
  }

  inCorrectUser() {
    this.postLoginDataToServer([0,this.user_id]).subscribe(
      res => {}
    );
  }

}
