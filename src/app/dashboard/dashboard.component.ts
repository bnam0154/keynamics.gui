import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  formLogin: FormGroup;
  chartimeVector = [];
  allTimeVector = [];
  startTimestamp = 0;
  stopTimestamp = 0;

  count = 1;

  progressBarMode ='determinate'
  

  keydowntime = 0;
  keyuptime = 0;
  keypresstimevector = [];
  allKeypresstimevector = []

  value = '';
  allowedWordsInOrder = ['i','in','ind','indu','indul', 'indul ', 'indul a', 'indul a ',
  'indul a p','indul a pa', 'indul a pap', 'indul a pap ', 'indul a pap a', 'indul a pap al',
  'indul a pap alu', 'indul a pap alud', 'indul a pap aludn', 'indul a pap aludni' ];

  // cors = "https://cors-anywhere.herokuapp.com/";
  // baseURL = this.cors + "https://keynamics-core.herokuapp.com/";
  baseURL = "http://localhost:5000/"

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient, 
    private router: Router
    ) { 
      this.formLogin = this.formBuilder.group({
        passcode: ['', Validators.required],
        
      });
    }

  ngOnInit() {

  }

  resetTypeWord() {
    this.formLogin.patchValue({passcode: ''});
    this.formLogin.updateValueAndValidity();
  }

  onKeydown() {
    this.keydowntime = Date.now();
  }

  onKeyup(event) {
    
    this.value += event.key;
    if(this.value != this.allowedWordsInOrder[this.value.length-1]) {
      this.value = '';
      this.chartimeVector = [];
      this.keypresstimevector = [];
      this.resetTypeWord();
    } else {

      this.keyuptime = Date.now();
      this.keypresstimevector.push([event.key, this.keyuptime - this.keydowntime]);
      
      if(this.value.length == 1) {
        // elso alkalommal
        this.startTimestamp = Date.now();
      } else {
        this.chartimeVector.push(Date.now() - this.startTimestamp);
        this.startTimestamp = Date.now();
      }

      if (this.value == 'indul a pap aludni') {
        this.count = this.count-1;
        this.allTimeVector.push(this.chartimeVector);
        this.allKeypresstimevector.push(this.keypresstimevector);
        this.chartimeVector = [];
        this.keypresstimevector = [];
        this.resetTypeWord();
        this.value = '';
      }
    }
    if(this.count == 0) {
      this.progressBarMode = 'indeterminate'
      this.value = '';
      this.chartimeVector = [];
      this.keypresstimevector = [];
      this.resetTypeWord();
      let blob = new Blob([JSON.stringify(this.formLogin.value), '\n', JSON.stringify(this.allTimeVector), '\n', JSON.stringify(this.allKeypresstimevector)], {type: 'text/plain'});
      console.log(blob)
      this.postDataToServer(blob).subscribe(
        res => {
          this.router.navigateByUrl('/mainuserpage?userName=' + res[2].predicted_user
          + '&keystroke_data=' + [JSON.stringify(this.allTimeVector), '\n', JSON.stringify(this.allKeypresstimevector)]
          + '&predicted_id=' + res[1].predicted_id
          + '&score=' + res[3].score
          + '&log_prob=' + res[4].log_prob
          + '&prob=' + res[5].prob);
          this.progressBarMode ='determinate'
        }
      );
      this.count = 1;
    }
      
  }

  login() {
    console.log("logni");
  }

  postDataToServer (data: Blob): Observable<any> {
    return this.http.post(this.baseURL + 'login', data)
  }

  register() {

  }

}
