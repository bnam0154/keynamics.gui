import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatSidenavModule, MatCardModule, MatButtonModule, MatMenuModule, MatIconModule } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { DataCollecterComponent } from './data-collecter/data-collecter.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LayoutComponent } from './shared/components/layout/layout.component';
import { NavigationComponent } from './shared/components/navigation/navigation.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './shared/components/header/header.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatStepperModule } from '@angular/material/stepper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { MainuserpageComponent } from './mainuserpage/mainuserpage.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ControlpanelComponent } from './controlpanel/controlpanel.component';
import { MatPaginatorModule } from '@angular/material';

const appRoutes: Routes = [
  { path: 'data-collecter', component: DataCollecterComponent},
  { path: 'mainuserpage', component: MainuserpageComponent}, 
  { path: 'controlpanel', component: ControlpanelComponent}, 
  { path: '', component: DashboardComponent}
  
];

@NgModule({
  declarations: [
    AppComponent,
    DataCollecterComponent,
    DashboardComponent,
    LayoutComponent,
    NavigationComponent,
    HeaderComponent,
    MainuserpageComponent,
    ControlpanelComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    BrowserAnimationsModule,
    ReactiveFormsModule, FormsModule,
    MatSidenavModule, MatDialogModule, MatStepperModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatDividerModule,
    MatGridListModule, 
    MatTableModule, 
    HttpClientModule, 
    MatProgressBarModule, MatPaginatorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
