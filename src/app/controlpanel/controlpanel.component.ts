import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ɵAnimationGroupPlayer } from '@angular/animations';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { stringify } from 'querystring';

@Component({
  selector: 'app-controlpanel',
  templateUrl: './controlpanel.component.html',
  styleUrls: ['./controlpanel.component.scss']
})
export class ControlpanelComponent implements OnInit {

  // cors = "https://cors-anywhere.herokuapp.com/";
  // baseURL = this.cors + "https://keynamics-core.herokuapp.com/";
  baseURL = "http://localhost:5000/"
  
  nrOfUsers = 0;
  
  displayedColumns: string[] = ['id', 'name', 'nrofdata', 'correctlogins', 'incorrectlogins', 'totallogins'];
  dataSource = new MatTableDataSource;
  
  dataToBeTrained = 0;

  constructor(
    private http: HttpClient
  ) { }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  
  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.getInfoDB().subscribe(
      res => {
        this.nrOfUsers = parseInt(res[0].totalNrOfUsers, 10);
        
        for (let i = 0; i < this.nrOfUsers; i++) {
          this.dataSource.data.push(
            {
            id: parseInt(res[0].allUserId[i],10), 
            name: res[0].allUserNames[i], 
            nrofdata: res[0].allNumberOfData[i],
            correctlogins: res[0].loginInformation[i][2],
            incorrectlogins: res[0].loginInformation[i][3],
            totallogins: res[0].loginInformation[i][1],
          })
        }

        this.dataSource.data = [...this.dataSource.data];
        this.dataToBeTrained = res[0].nrAllUntrainedData
      }
    );
  }

  getInfoDB(): Observable<any> {
    return this.http.get(this.baseURL + 'getinfodb')
  }

  saveToFile(): Observable<any> {
    return this.http.get(this.baseURL + 'savedbtocsv')
  }
    
  resetDatabase(): Observable<any> {
    return this.http.get(this.baseURL + 'resetdatabase')
  }

  trainuntraineddata(): Observable<any> {
    return this.http.get(this.baseURL + 'trainuntraineddata')
  }

  resetdatabase() {
    this.resetDatabase().subscribe(
      res => {
        this.dataSource.data = [...this.dataSource.data];
    });
  }

  savetofile() {
    this.saveToFile().subscribe(
      res => {
        let date = new Date();
        let blob = new Blob([JSON.stringify(res)]);
        let link = document.createElement('a');
        link.download = 'keynamicsDB_' + date.getDate() + "_" + date.getMonth() + "_" + date.getHours() + "_" + date.getMinutes() + '.kdb';
        link.href = URL.createObjectURL(blob);
        link.click();
      });
  }

  train() {
    this.trainuntraineddata().subscribe(
      res => {}
    );
    // this.getInfoDB().subscribe(
    //   res => {
    //     this.nrOfUsers = parseInt(res[0].totalNrOfUsers, 10);
        
    //     for (let i = 0; i < this.nrOfUsers; i++) {
    //       this.dataSource.data.push(
    //         {
    //         id: parseInt(res[0].allUserId[i],10), 
    //         name: res[0].allUserNames[i], 
    //         nrofdata: res[0].allNumberOfData[i],
    //         correctlogins: res[0].loginInformation[i][2],
    //         incorrectlogins: res[0].loginInformation[i][3],
    //         totallogins: res[0].loginInformation[i][1],
    //       })
    //     }

    //     this.dataSource.data = [...this.dataSource.data];
    //     this.dataToBeTrained = res[0].nrAllUntrainedData
    //   }
    // );
  }

}
