import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { MatStepper } from '@angular/material';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-data-collecter',
  templateUrl: './data-collecter.component.html',
  styleUrls: ['./data-collecter.component.scss']
})
export class DataCollecterComponent implements OnInit {

  formCollecting: FormGroup;
  // cors = "https://cors-anywhere.herokuapp.com/";
  // baseURL = this.cors + "https://keynamics-core.herokuapp.com/";
  baseURL = "http://localhost:5000/"

  chartimeVector = [];
  allTimeVector = [];
  startTimestamp = 0;
  stopTimestamp = 0;
  
  sentence = "indul a pap aludni";
  howMany = 10;
  iteration = 0;

  keydowntime = 0;
  keyuptime = 0;
  keypresstimevector = [];
  allKeypresstimevector = []
  
  value = '';
  allowedWordsInOrder = ['i','in','ind','indu','indul', 'indul ', 'indul a', 'indul a ',
  'indul a p','indul a pa', 'indul a pap', 'indul a pap ', 'indul a pap a', 'indul a pap al',
  'indul a pap alu', 'indul a pap alud', 'indul a pap aludn', 'indul a pap aludni' ];

  constructor(
    private formBuilder: FormBuilder, 
    private http: HttpClient
  ) { 
    this.formCollecting = this.formBuilder.group({
      name: ['', Validators.required],
      age: ['', Validators.required],
      gender: ['', Validators.required],
      word: [''],
      numberofcoll: [this.howMany, Validators.required],
      typeword: [''],
      count: [this.howMany, Validators.required]
      
    });
  }
  
  ngOnInit() {
    
  }

  resetTypeWord() {
    this.formCollecting.patchValue({typeword: ''});
    this.formCollecting.updateValueAndValidity();
  }

  changedNumberOfColl(value) {
    this.formCollecting.patchValue({count: value});
  }

  nextStepper(stepper: MatStepper) {
    stepper.next();
  }

  save() {
    // this.counterIsNull = false;
    if (this.allTimeVector.length){
      let date = new Date();
      // nev_nap_honap_ora_perc.txt
      var fileName = this.formCollecting.value.name + "_" + date.getDate() + "_" + date.getMonth() + "_" + date.getHours() + "_" + date.getMinutes() + ".txt";
      let blob = new Blob([JSON.stringify(this.formCollecting.value), '\n', JSON.stringify(this.allTimeVector), '\n', JSON.stringify(this.allKeypresstimevector)], {type: 'text/plain'});
      // let link = document.createElement('a');
      // link.download = fileName;
      // link.href = URL.createObjectURL(blob);
      // link.click();
      this.postDataToServer(blob, fileName).subscribe(
        res => { console.log(res);}
      );
    }
  }

  postDataToServer (data: Blob, fileName: string): Observable<any> {
    // return this.http.post(this.baseURL + fileName, data)
    return this.http.post(this.baseURL + 'keystrokesdata', data)
  }

  onKeydown() {
    this.keydowntime = Date.now();
  }

  onKeyup(event, stepper: MatStepper) {
    
    this.value += event.key;
    console.log(this.value);
    console.log(this.allowedWordsInOrder[this.value.length-1]);
    if(this.value != this.allowedWordsInOrder[this.value.length-1]) {
      this.value = '';
      this.chartimeVector = [];
      this.keypresstimevector = [];
      this.resetTypeWord();
      this.value = '';
    } else {

      this.keyuptime = Date.now();
      this.keypresstimevector.push([event.key, this.keyuptime - this.keydowntime]);
      
      if(this.value.length == 1) {
        // elso alkalommal
        this.startTimestamp = Date.now();
      } else {
        this.chartimeVector.push(Date.now() - this.startTimestamp);
        this.startTimestamp = Date.now();
      }

      if (this.value == 'indul a pap aludni') {
        this.formCollecting.patchValue({count: this.formCollecting.value.count-1})
        this.allTimeVector.push(this.chartimeVector);
        this.allKeypresstimevector.push(this.keypresstimevector);
        this.chartimeVector = [];
        this.keypresstimevector = [];
        this.resetTypeWord();
        this.value = '';
      }
    }
    if(this.formCollecting.value.count == 0) {
      // this.counterIsNull = true;
      this.nextStepper(stepper);
    }  
  }

}
