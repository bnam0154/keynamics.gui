import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataCollecterComponent } from './data-collecter.component';

describe('DataCollecterComponent', () => {
  let component: DataCollecterComponent;
  let fixture: ComponentFixture<DataCollecterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataCollecterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataCollecterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
