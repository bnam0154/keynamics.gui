(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<router-outlet></router-outlet>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/controlpanel/controlpanel.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/controlpanel/controlpanel.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div style=\"background-color: #f6f6f6; height: 100%\" >\n    <mat-card class=\"wfull\" style=\"background: #3f51b5\" class=\"z-depth-2\">\n        <div style=\"float: right\">\n            <button mat-raised-button \n            color=\"primary\"\n            routerLink=\"/\"><mat-icon>keyboard_backspace</mat-icon>back</button>\n        </div>\n        <div style=\"float: left\">\n            <span class=\"leftpad21\" style=\"color: white;font-family:courier; font-size: 1.5rem;\"><b>Control Panel</b></span>\n        </div>\n    </mat-card>\n\n    <div>\n        <mat-card class=\"matcardBlock z-depth-2\" style=\"margin-top: 3rem\">\n            <mat-card-content>\n                <h2>Keystroke Data</h2>\n                <div style=\"margin-top: 2rem\">\n                    <h2 style=\"text-align: left;font-weight:normal\"><span style=\"font-weight:bold\">Untrained: </span>{{dataToBeTrained}}</h2>\n                    <span style=\"font-size: 1rem; font-weight: bold;\">*training data collected on user login, that has not been processed</span>\n                </div>\n                <div style=\"margin-top: 2rem;\">\n                <button mat-raised-button \n                    color=\"primary\"\n                    (click)=\"train()\"><mat-icon>donut_large</mat-icon>Train</button>\n                </div>\n            </mat-card-content>\n        </mat-card>\n\n    <mat-card class=\"matcardBlock z-depth-2\" style=\"margin-top: 10rem\">\n        <mat-card-content>\n            <h2>Database management</h2>\n            <div style=\"margin-top: 2rem\">\n                <h2 style=\"text-align: left;font-weight:normal\"><span style=\"font-weight:bold\">Total users:</span> {{ nrOfUsers }}</h2>\n            </div>\n            <div class=\"mat-elevation-z8\">\n                <table mat-table [dataSource]=\"dataSource\">\n                    <ng-container matColumnDef=\"id\">\n                        <th mat-header-cell *matHeaderCellDef><b style=\"font-size: 1rem;\"> Id </b></th>\n                        <td mat-cell *matCellDef=\"let element\"> {{element.id}} </td>\n                    </ng-container>\n                    <ng-container matColumnDef=\"name\">\n                        <th mat-header-cell *matHeaderCellDef><b style=\"font-size: 1rem;\"> Name </b></th>\n                        <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\n                    </ng-container>\n                    <ng-container matColumnDef=\"nrofdata\">\n                        <th mat-header-cell *matHeaderCellDef><b style=\"font-size: 1rem;\"> Nr. Data </b></th>\n                        <td mat-cell *matCellDef=\"let element\"> {{element.nrofdata}} </td>\n                    </ng-container>\n                    <ng-container matColumnDef=\"correctlogins\">\n                        <th mat-header-cell *matHeaderCellDef><b style=\"font-size: 1rem;\"> Correct logins </b></th>\n                        <td mat-cell *matCellDef=\"let element\"> {{element.correctlogins}} </td>\n                    </ng-container>\n                    <ng-container matColumnDef=\"incorrectlogins\">\n                        <th mat-header-cell *matHeaderCellDef><b style=\"font-size: 1rem;\"> Incorrect logins </b></th>\n                        <td mat-cell *matCellDef=\"let element\"> {{element.incorrectlogins}} </td>\n                    </ng-container>\n                    <ng-container matColumnDef=\"totallogins\">\n                        <th mat-header-cell *matHeaderCellDef><b style=\"font-size: 1rem;\"> Total logins </b></th>\n                        <td mat-cell *matCellDef=\"let element\"> {{element.totallogins}} </td>\n                    </ng-container>\n                    <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\n                    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n                </table>\n                <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n            </div>\n        </mat-card-content>\n\n        <mat-card-content>\n            <div>\n                <button mat-raised-button \n                color=\"warn\"\n                (click)=\"resetdatabase()\"><mat-icon>settings_backup_restore</mat-icon>Reset database</button>\n                <button mat-raised-button \n                style=\"margin-left: 1rem;\"\n                color=\"primary\"\n                (click)=\"savetofile()\"><mat-icon>save_alt</mat-icon>Save to file</button>\n            </div>\n            \n        </mat-card-content>\n        <mat-divider></mat-divider>\n    </mat-card>\n    </div>\n    \n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div style=\"background-color: #f6f6f6; height: 100%\">\n    <mat-card class=\"wfull\" style=\"background: #3f51b5\"\n    class=\"z-depth-2\">\n    <div style=\"float: right\">\n        <button mat-raised-button \n        color=\"primary\"\n        routerLink=\"/controlpanel\"><span style=\"color: white\">Control Panel</span></button>\n    </div>\n    <div style=\"float: left\">\n        <span class=\"leftpad21\" style=\"color: white;font-family:courier; font-size: 1.5rem;\"><b>Keynamics</b></span>\n    </div>\n    </mat-card>\n\n    <mat-card class=\"matcardBlock z-depth-2\" style=\"margin-top: 15rem\">\n        <mat-progress-bar mode=\"{{progressBarMode}}\" value=\"{{(2-count)*50}}\" color=\"primary\"></mat-progress-bar>\n        <mat-card-content>\n            <form [formGroup]=\"formLogin\">\n                <h1>Log In</h1>\n                <h3>The passcode is:</h3>\n                <h3><p style=\"color: #f44336\">indul a pap aludni</p></h3>\n                <mat-form-field class=\"wfull\" style=\"margin-top: 5rem\" hintLabel=\"Entered the passcode 2 times\">\n                    <input matInput placeholder=\"Pass Code\" \n                    formControlName=\"passcode\"\n                    (keydown)=\"onKeydown()\"\n                    (keyup)=\"onKeyup($event)\" \n                    autocomplete=\"off\" required>\n                    <!-- <mat-error style=\"color: #f44336\">\n                      Please provide a valid passcode\n                    </mat-error> -->\n                    <mat-hint align=\"end\">{{2-count || 0}}/2</mat-hint>\n                </mat-form-field>\n                <div style=\"margin-top: 5rem\">\n                    <button mat-raised-button \n                    color=\"primary\"\n                    (click)=\"login()\">Help</button>\n                    <button mat-raised-button \n                    color=\"warn\" style =\"margin-left:1rem\"\n                    (click)=\"register()\"\n                    routerLink=\"/data-collecter\">Register</button>\n                </div>\n            </form>\n        </mat-card-content>\n    </mat-card>\n\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/data-collecter/data-collecter.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/data-collecter/data-collecter.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <app-layout></app-layout> -->\n<div style=\"background-color: #f6f6f6; height: 100%;\">\n<mat-card class=\"wfull\" style=\"background: #3f51b5\" class=\"z-depth-2\">\n  <div style=\"float: right\">\n    <button mat-raised-button \n    color=\"primary\"\n    routerLink=\"/\"><mat-icon>keyboard_backspace</mat-icon>back</button>\n  </div>\n  <div style=\"float: left\">\n    <span class=\"leftpad21\" style=\"color: white;font-family:courier; font-size: 1.5rem;\"><b>Data Collecter page</b></span>\n  </div>\n</mat-card>\n<mat-horizontal-stepper [linear]=\"false\" style=\"margin-top: 4rem; background-color: #f6f6f6\" #stepper>\n    <mat-step [stepControl]=\"formCollecting\">\n      <form [formGroup]=\"formCollecting\">\n        <mat-card>\n          <h5>*All fields are mandatory</h5>\n          <ng-template matStepLabel>Fill out your information</ng-template>\n          <mat-form-field class=\"wfull\">\n            <mat-label>Name</mat-label>\n            <input matInput placeholder=\"Last name, First name\" autocomplete=\"off\" formControlName=\"name\" required>\n          </mat-form-field>\n          <mat-divider></mat-divider>\n          <mat-form-field class=\"wfull\">\n              <mat-label>Age</mat-label>\n              <input matInput type=\"number\" placeholder=\"Age\" autocomplete=\"off\" formControlName=\"age\" required>\n          </mat-form-field>\n          <mat-divider></mat-divider>\n          <mat-form-field class=\"wfull\">\n            <mat-select required formControlName=\"gender\" placeholder=\"Gender\">\n              <mat-option value=\"Male\">Male</mat-option>\n              <mat-option value=\"Female\">Female</mat-option>\n              <mat-option value=\"Other\">Other</mat-option>\n            </mat-select>\n          </mat-form-field>\n          <div>\n            <button mat-button matStepperNext>Next</button>\n          </div>\n        </mat-card>\n      </form>\n    </mat-step>\n    <mat-step [stepControl]=\"formCollecting\">\n      <form [formGroup]=\"formCollecting\">\n        <ng-template matStepLabel>Collecting point</ng-template>\n        <mat-card>\n          <h2>Type the given sentence:  <span style=\"color: #f44336\">indul a pap aludni</span></h2>\n          The given sentence needs to be entered the number of times shown below. If a mistake is made the sentence is deleted automatically and the user is asked to retype the passcode.\n        </mat-card>\n        <mat-divider></mat-divider>\n        <mat-card>\n          <mat-form-field class=\"wfull leftpad8\">\n              <mat-label>Number of times to collect</mat-label>\n              <input matInput [readonly]=\"true\" type=\"number\" placeholder=\"Ex. 20\" \n              formControlName=\"numberofcoll\" \n              (input)=\"changedNumberOfColl($event.target.value)\" \n              autocomplete=\"off\" required>\n          </mat-form-field>\n          <mat-divider></mat-divider>\n          <mat-form-field appearance=\"fill\" class=\"wquarter\">\n              <mat-label>Type here</mat-label>\n              <div *ngIf=\"counterIsNull==true; then withReadOnly else withoutReadOnly\"></div>\n                  <ng-template #withoutReadOnly>\n                      <input matInput formControlName=\"typeword\" \n                      (keydown)=\"onKeydown()\"\n                      (keyup)=\"onKeyup($event, stepper)\" \n                      autocomplete=\"off\" required>\n                  </ng-template>\n                  <!-- <ng-template #withReadOnly>\n                      <input matInput formControlName=\"typeword\"\n                      [readonly]=\"true\" placeholder=\"Finished\" \n                      autocomplete=\"off\" required>\n                  </ng-template> -->\n                  \n          </mat-form-field>\n          <mat-form-field class=\"wthird leftpad8\">\n              <mat-label>Count</mat-label>\n              <input matInput type=\"number\" [readonly]=\"true\" autocomplete=\"off\" formControlName=\"count\">\n          </mat-form-field>\n          <mat-divider></mat-divider>\n          <div>\n            <button mat-button matStepperPrevious>Back</button>\n            <button mat-button matStepperNext (click)=\"resetCounterIsNull()\">Next</button>\n          </div>\n        </mat-card>\n      </form>\n    </mat-step>\n    <mat-step>\n      <mat-card>\n        <ng-template matStepLabel>Done</ng-template>\n        <h2><p style=\"text-align: center\">You are now done<mat-icon style=\"color:green; font-size: 50px\">done_all</mat-icon></p></h2>\n        <div style=\"text-align: center; margin-top: 5rem;\">\n          <button mat-raised-button \n          color=\"primary\" (click)=\"save()\"\n          routerLink=\"/\">Save</button>\n          <button mat-raised-button \n          color=\"warn\" style =\"margin-left:1rem\" \n          (click)=\"stepper.reset()\">Reset</button>\n        </div>\n      </mat-card>\n      <mat-divider></mat-divider>\n      <mat-card>\n        <h3 style=\"font-weight:normal; text-align: center\">Your data has been collected. Please click <span style=\"font-weight:bold; color: #3f51b5\">save</span> to register your keystroke dynamic, or <span style=\"font-weight:bold; color: #f44336\">reset</span> to restart</h3>\n      </mat-card>\n    </mat-step>\n</mat-horizontal-stepper>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/mainuserpage/mainuserpage.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/mainuserpage/mainuserpage.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div style=\"background-color: #f6f6f6; height: 100%\" >\n    <mat-card class=\"wfull\" style=\"background: #3f51b5\" class=\"z-depth-2\">\n        <h2 mat-dilog-title class=\"leftpad21\" style=\"color: white;font-family:courier\">Main user page: <span style=\"font-size:20px\">{{userName}}</span></h2>\n    </mat-card>\n\n    <mat-card class=\"matcardBlock z-depth-2\" style=\"margin-top: 5rem\">\n        <mat-card-content>\n            <h3>User successfully logged in</h3>\n            <mat-divider style=\"border-top-width: 2rem;\n            border-top-color: transparent\"></mat-divider>\n            <div style=\"margin-top: 5rem\">\n                <h2 style=\"text-align: left;font-weight:normal\"><span style=\"font-weight:bold\">User Name:</span> {{ userName }}</h2>\n                <mat-divider></mat-divider>\n                <div style=\"margin-top: 2rem\">\n                <!-- <h2 style=\"text-align: left;font-weight:normal\"><span style=\"font-weight:bold\">Score:</span> {{ score }}</h2> -->\n                <!-- <h2 style=\"text-align: left;font-weight:normal\"><span style=\"font-weight:bold\">Log probability:</span> {{ log_prob }}</h2> -->\n                <!-- <h2 style=\"text-align: left;font-weight:normal\"><span style=\"font-weight:bold\">Probability:</span> {{ prob }}</h2> -->\n                </div>\n            </div>\n            <mat-divider></mat-divider>\n            <mat-divider style=\"border-top-width: 2rem;\n            border-top-color: transparent\"></mat-divider>\n            <!-- <div style=\"margin-top: 5rem\">\n                <button mat-raised-button \n                        color=\"primary\"\n                        routerLink=\"/\">\n                        Logout\n                </button> -->\n            <!-- </div> -->\n        </mat-card-content>\n    </mat-card>\n\n    <mat-card class=\"matcardBlock z-depth-2\" style=\"margin-top: 5rem; height: 10rem\">\n        <mat-card-content>\n            <h3>Please give us a feedback</h3>\n            <h3 style=\"font-weight:normal\">Is it your user page?</h3>\n            <div style=\"margin-top: 3rem\">\n                <button mat-raised-button \n                        style=\"color:green\"\n                        (click)=\"correctUser()\"\n                        routerLink=\"/\">\n                        Yes\n                </button>\n                <button mat-raised-button \n                        style=\"color:red; margin-left: 1rem\"\n                        (click)=\"inCorrectUser()\"\n                        routerLink=\"/\">\n                        No\n                </button>\n            </div>\n        </mat-card-content>\n    </mat-card>\n\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/header/header.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/header/header.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>header works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/layout/layout.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/layout/layout.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-navigation></app-navigation>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/navigation/navigation.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/navigation/navigation.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-menu #appMenu=\"matMenu\">\n    <button routerLink=\"/data-collecter\" mat-menu-item>\n        <mat-icon>accessibility</mat-icon>\n        Collect Data\n    </button>\n    <button mat-menu-item>Settings</button>\n    <button mat-menu-item>Help</button>\n  </mat-menu>\n  \n  <button mat-icon-button [matMenuTriggerFor]=\"appMenu\">\n    <mat-icon>more_vert</mat-icon>\n  </button>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".width {\n  width: 100%;\n}\n\n.whalf {\n  width: 50%;\n}\n\n.wquarter {\n  width: 75%;\n}\n\n.wthird {\n  width: 24.4%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvRDpcXF9VQkJcXExpY2Vuc3pcXF9fX19BbGxhbXZpenNnYSBnaXRcXGFsbGFtdml6c2dhLWJuYW0wMTU0LXViYi0yMDIwXFxrZXluYW1pY3Mvc3JjXFxhcHBcXGFwcC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndpZHRoIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ud2hhbGYge1xyXG4gICAgd2lkdGg6IDUwJTtcclxufVxyXG5cclxuLndxdWFydGVyIHtcclxuICAgIHdpZHRoOiA3NSU7XHJcbn1cclxuXHJcbi53dGhpcmQge1xyXG4gICAgd2lkdGg6IDI0LjQlO1xyXG59IiwiLndpZHRoIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi53aGFsZiB7XG4gIHdpZHRoOiA1MCU7XG59XG5cbi53cXVhcnRlciB7XG4gIHdpZHRoOiA3NSU7XG59XG5cbi53dGhpcmQge1xuICB3aWR0aDogMjQuNCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'keynamics';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _data_collecter_data_collecter_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./data-collecter/data-collecter.component */ "./src/app/data-collecter/data-collecter.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _shared_components_layout_layout_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./shared/components/layout/layout.component */ "./src/app/shared/components/layout/layout.component.ts");
/* harmony import */ var _shared_components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared/components/navigation/navigation.component */ "./src/app/shared/components/navigation/navigation.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _shared_components_header_header_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shared/components/header/header.component */ "./src/app/shared/components/header/header.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm2015/stepper.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm2015/form-field.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm2015/select.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/esm2015/divider.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm2015/grid-list.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _mainuserpage_mainuserpage_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./mainuserpage/mainuserpage.component */ "./src/app/mainuserpage/mainuserpage.component.ts");
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/progress-bar */ "./node_modules/@angular/material/esm2015/progress-bar.js");
/* harmony import */ var _controlpanel_controlpanel_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./controlpanel/controlpanel.component */ "./src/app/controlpanel/controlpanel.component.ts");




























const appRoutes = [
    { path: 'data-collecter', component: _data_collecter_data_collecter_component__WEBPACK_IMPORTED_MODULE_7__["DataCollecterComponent"] },
    { path: 'mainuserpage', component: _mainuserpage_mainuserpage_component__WEBPACK_IMPORTED_MODULE_22__["MainuserpageComponent"] },
    { path: 'controlpanel', component: _controlpanel_controlpanel_component__WEBPACK_IMPORTED_MODULE_24__["ControlpanelComponent"] },
    { path: '', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_8__["DashboardComponent"] }
];
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _data_collecter_data_collecter_component__WEBPACK_IMPORTED_MODULE_7__["DataCollecterComponent"],
            _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_8__["DashboardComponent"],
            _shared_components_layout_layout_component__WEBPACK_IMPORTED_MODULE_9__["LayoutComponent"],
            _shared_components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_10__["NavigationComponent"],
            _shared_components_header_header_component__WEBPACK_IMPORTED_MODULE_13__["HeaderComponent"],
            _mainuserpage_mainuserpage_component__WEBPACK_IMPORTED_MODULE_22__["MainuserpageComponent"],
            _controlpanel_controlpanel_component__WEBPACK_IMPORTED_MODULE_24__["ControlpanelComponent"]
        ],
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forRoot(appRoutes),
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_11__["CommonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatMenuModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__["BrowserAnimationsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_16__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_16__["FormsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenavModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_14__["MatDialogModule"], _angular_material_stepper__WEBPACK_IMPORTED_MODULE_15__["MatStepperModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_18__["MatSelectModule"], _angular_material_divider__WEBPACK_IMPORTED_MODULE_19__["MatDividerModule"],
            _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_20__["MatGridListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_21__["HttpClientModule"],
            _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_23__["MatProgressBarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginatorModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/controlpanel/controlpanel.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/controlpanel/controlpanel.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table {\n  width: 100%;\n}\n\n.leftpad21 {\n  padding-left: 21px;\n}\n\n.z-depth-2 {\n  box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);\n  height: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udHJvbHBhbmVsL0Q6XFxfVUJCXFxMaWNlbnN6XFxfX19fQWxsYW12aXpzZ2EgZ2l0XFxhbGxhbXZpenNnYS1ibmFtMDE1NC11YmItMjAyMFxca2V5bmFtaWNzL3NyY1xcYXBwXFxjb250cm9scGFuZWxcXGNvbnRyb2xwYW5lbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29udHJvbHBhbmVsL2NvbnRyb2xwYW5lbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxnSEFBQTtFQUNBLFlBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2NvbnRyb2xwYW5lbC9jb250cm9scGFuZWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmxlZnRwYWQyMSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDIxcHg7XHJcbn1cclxuXHJcbi56LWRlcHRoLTIge1xyXG4gICAgYm94LXNoYWRvdzogMCA0cHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAxcHggMTBweCAwIHJnYmEoMCwgMCwgMCwgMC4xMiksIDAgMnB4IDRweCAtMXB4IHJnYmEoMCwgMCwgMCwgMC4zKTtcclxuICAgIGhlaWdodDogMjVweDtcclxufSIsInRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5sZWZ0cGFkMjEge1xuICBwYWRkaW5nLWxlZnQ6IDIxcHg7XG59XG5cbi56LWRlcHRoLTIge1xuICBib3gtc2hhZG93OiAwIDRweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDFweCAxMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKSwgMCAycHggNHB4IC0xcHggcmdiYSgwLCAwLCAwLCAwLjMpO1xuICBoZWlnaHQ6IDI1cHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/controlpanel/controlpanel.component.ts":
/*!********************************************************!*\
  !*** ./src/app/controlpanel/controlpanel.component.ts ***!
  \********************************************************/
/*! exports provided: ControlpanelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ControlpanelComponent", function() { return ControlpanelComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");




let ControlpanelComponent = class ControlpanelComponent {
    constructor(http) {
        this.http = http;
        this.cors = "https://cors-anywhere.herokuapp.com/";
        this.baseURL = this.cors + "https://keynamics-core.herokuapp.com/";
        this.nrOfUsers = 0;
        this.displayedColumns = ['id', 'name', 'nrofdata', 'correctlogins', 'incorrectlogins', 'totallogins'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"];
        this.dataToBeTrained = 0;
    }
    ngOnInit() {
        this.dataSource.paginator = this.paginator;
        this.getInfoDB().subscribe(res => {
            this.nrOfUsers = parseInt(res[0].totalNrOfUsers, 10);
            for (let i = 0; i < this.nrOfUsers; i++) {
                this.dataSource.data.push({
                    id: parseInt(res[0].allUserId[i], 10),
                    name: res[0].allUserNames[i],
                    nrofdata: res[0].allNumberOfData[i],
                    correctlogins: res[0].loginInformation[i][2],
                    incorrectlogins: res[0].loginInformation[i][3],
                    totallogins: res[0].loginInformation[i][1],
                });
            }
            this.dataSource.data = [...this.dataSource.data];
            this.dataToBeTrained = res[0].nrAllUntrainedData;
        });
    }
    getInfoDB() {
        return this.http.get(this.baseURL + 'getinfodb');
    }
    saveToFile() {
        return this.http.get(this.baseURL + 'savedbtocsv');
    }
    resetDatabase() {
        return this.http.get(this.baseURL + 'resetdatabase');
    }
    trainuntraineddata() {
        return this.http.get(this.baseURL + 'trainuntraineddata');
    }
    resetdatabase() {
        this.resetDatabase().subscribe(res => {
            this.dataSource.data = [...this.dataSource.data];
        });
    }
    savetofile() {
        this.saveToFile().subscribe(res => {
            let date = new Date();
            let blob = new Blob([JSON.stringify(res)]);
            let link = document.createElement('a');
            link.download = 'keynamicsDB_' + date.getDate() + "_" + date.getMonth() + "_" + date.getHours() + "_" + date.getMinutes() + '.kdb';
            link.href = URL.createObjectURL(blob);
            link.click();
        });
    }
    train() {
        this.trainuntraineddata().subscribe(res => { });
        // this.getInfoDB().subscribe(
        //   res => {
        //     this.nrOfUsers = parseInt(res[0].totalNrOfUsers, 10);
        //     for (let i = 0; i < this.nrOfUsers; i++) {
        //       this.dataSource.data.push(
        //         {
        //         id: parseInt(res[0].allUserId[i],10), 
        //         name: res[0].allUserNames[i], 
        //         nrofdata: res[0].allNumberOfData[i],
        //         correctlogins: res[0].loginInformation[i][2],
        //         incorrectlogins: res[0].loginInformation[i][3],
        //         totallogins: res[0].loginInformation[i][1],
        //       })
        //     }
        //     this.dataSource.data = [...this.dataSource.data];
        //     this.dataToBeTrained = res[0].nrAllUntrainedData
        //   }
        // );
    }
};
ControlpanelComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"], { static: true })
], ControlpanelComponent.prototype, "paginator", void 0);
ControlpanelComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-controlpanel',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./controlpanel.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/controlpanel/controlpanel.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./controlpanel.component.scss */ "./src/app/controlpanel/controlpanel.component.scss")).default]
    })
], ControlpanelComponent);



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.scss":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wfull {\n  width: 100%;\n  border: none !important;\n}\n\n.z-depth-2 {\n  box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);\n  height: 25px;\n}\n\n.whalf {\n  width: 50%;\n}\n\n.wquarter {\n  width: 75%;\n}\n\n.wthird {\n  width: 24.4%;\n}\n\n.mat-grid-tile {\n  background: #335d7e;\n}\n\n.matcardBlock {\n  max-width: 400px;\n  margin: 2em auto;\n  text-align: center;\n  height: 25rem;\n}\n\n.mat-form-field {\n  display: block;\n}\n\n.mat-divider {\n  border-top-width: 6rem;\n  border-top-color: transparent;\n}\n\n.leftpad21 {\n  padding-left: 21px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL0Q6XFxfVUJCXFxMaWNlbnN6XFxfX19fQWxsYW12aXpzZ2EgZ2l0XFxhbGxhbXZpenNnYS1ibmFtMDE1NC11YmItMjAyMFxca2V5bmFtaWNzL3NyY1xcYXBwXFxkYXNoYm9hcmRcXGRhc2hib2FyZC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksZ0hBQUE7RUFDQSxZQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0FDQ0o7O0FERUE7RUFDSSxZQUFBO0FDQ0o7O0FERUE7RUFDSSxtQkFBQTtBQ0NKOztBREVBO0VBRUksZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtBQ0FKOztBREdBO0VBQ0ksY0FBQTtBQ0FKOztBREdBO0VBQ0ksc0JBQUE7RUFDQSw2QkFBQTtBQ0FKOztBREdBO0VBQ0ksa0JBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2Z1bGwge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnotZGVwdGgtMiB7XHJcbiAgICBib3gtc2hhZG93OiAwIDRweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDFweCAxMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKSwgMCAycHggNHB4IC0xcHggcmdiYSgwLCAwLCAwLCAwLjMpO1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG59XHJcblxyXG4ud2hhbGYge1xyXG4gICAgd2lkdGg6IDUwJTtcclxufVxyXG5cclxuLndxdWFydGVyIHtcclxuICAgIHdpZHRoOiA3NSU7XHJcbn1cclxuXHJcbi53dGhpcmQge1xyXG4gICAgd2lkdGg6IDI0LjQlO1xyXG59XHJcblxyXG4ubWF0LWdyaWQtdGlsZSB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMzM1ZDdlO1xyXG59XHJcblxyXG4ubWF0Y2FyZEJsb2NrIHtcclxuICAgIC8vIHBhZGRpbmctdG9wOiA0NXB4O1xyXG4gICAgbWF4LXdpZHRoOiA0MDBweDtcclxuICAgIG1hcmdpbjogMmVtIGF1dG87XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBoZWlnaHQ6IDI1cmVtO1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5tYXQtZGl2aWRlciB7XHJcbiAgICBib3JkZXItdG9wLXdpZHRoOiA2cmVtO1xyXG4gICAgYm9yZGVyLXRvcC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbn1cclxuXHJcbi5sZWZ0cGFkMjEge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAyMXB4O1xyXG59IiwiLndmdWxsIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4uei1kZXB0aC0yIHtcbiAgYm94LXNoYWRvdzogMCA0cHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAxcHggMTBweCAwIHJnYmEoMCwgMCwgMCwgMC4xMiksIDAgMnB4IDRweCAtMXB4IHJnYmEoMCwgMCwgMCwgMC4zKTtcbiAgaGVpZ2h0OiAyNXB4O1xufVxuXG4ud2hhbGYge1xuICB3aWR0aDogNTAlO1xufVxuXG4ud3F1YXJ0ZXIge1xuICB3aWR0aDogNzUlO1xufVxuXG4ud3RoaXJkIHtcbiAgd2lkdGg6IDI0LjQlO1xufVxuXG4ubWF0LWdyaWQtdGlsZSB7XG4gIGJhY2tncm91bmQ6ICMzMzVkN2U7XG59XG5cbi5tYXRjYXJkQmxvY2sge1xuICBtYXgtd2lkdGg6IDQwMHB4O1xuICBtYXJnaW46IDJlbSBhdXRvO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGhlaWdodDogMjVyZW07XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4ubWF0LWRpdmlkZXIge1xuICBib3JkZXItdG9wLXdpZHRoOiA2cmVtO1xuICBib3JkZXItdG9wLWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cblxuLmxlZnRwYWQyMSB7XG4gIHBhZGRpbmctbGVmdDogMjFweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let DashboardComponent = class DashboardComponent {
    constructor(formBuilder, http, router) {
        this.formBuilder = formBuilder;
        this.http = http;
        this.router = router;
        this.chartimeVector = [];
        this.allTimeVector = [];
        this.startTimestamp = 0;
        this.stopTimestamp = 0;
        this.count = 1;
        this.progressBarMode = 'determinate';
        this.keydowntime = 0;
        this.keyuptime = 0;
        this.keypresstimevector = [];
        this.allKeypresstimevector = [];
        this.value = '';
        this.allowedWordsInOrder = ['i', 'in', 'ind', 'indu', 'indul', 'indul ', 'indul a', 'indul a ',
            'indul a p', 'indul a pa', 'indul a pap', 'indul a pap ', 'indul a pap a', 'indul a pap al',
            'indul a pap alu', 'indul a pap alud', 'indul a pap aludn', 'indul a pap aludni'];
        this.cors = "https://cors-anywhere.herokuapp.com/";
        this.baseURL = this.cors + "https://keynamics-core.herokuapp.com/";
        this.formLogin = this.formBuilder.group({
            passcode: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
    }
    ngOnInit() {
    }
    resetTypeWord() {
        this.formLogin.patchValue({ passcode: '' });
        this.formLogin.updateValueAndValidity();
    }
    onKeydown() {
        this.keydowntime = Date.now();
    }
    onKeyup(event) {
        this.value += event.key;
        if (this.value != this.allowedWordsInOrder[this.value.length - 1]) {
            this.value = '';
            this.chartimeVector = [];
            this.keypresstimevector = [];
            this.resetTypeWord();
        }
        else {
            this.keyuptime = Date.now();
            this.keypresstimevector.push([event.key, this.keyuptime - this.keydowntime]);
            if (this.value.length == 1) {
                // elso alkalommal
                this.startTimestamp = Date.now();
            }
            else {
                this.chartimeVector.push(Date.now() - this.startTimestamp);
                this.startTimestamp = Date.now();
            }
            if (this.value == 'indul a pap aludni') {
                this.count = this.count - 1;
                this.allTimeVector.push(this.chartimeVector);
                this.allKeypresstimevector.push(this.keypresstimevector);
                this.chartimeVector = [];
                this.keypresstimevector = [];
                this.resetTypeWord();
                this.value = '';
            }
        }
        if (this.count == 0) {
            this.progressBarMode = 'indeterminate';
            this.value = '';
            this.chartimeVector = [];
            this.keypresstimevector = [];
            this.resetTypeWord();
            let blob = new Blob([JSON.stringify(this.formLogin.value), '\n', JSON.stringify(this.allTimeVector), '\n', JSON.stringify(this.allKeypresstimevector)], { type: 'text/plain' });
            console.log(blob);
            this.postDataToServer(blob).subscribe(res => {
                this.router.navigateByUrl('/mainuserpage?userName=' + res[2].predicted_user
                    + '&keystroke_data=' + [JSON.stringify(this.allTimeVector), '\n', JSON.stringify(this.allKeypresstimevector)]
                    + '&predicted_id=' + res[1].predicted_id
                    + '&score=' + res[3].score
                    + '&log_prob=' + res[4].log_prob
                    + '&prob=' + res[5].prob);
                this.progressBarMode = 'determinate';
            });
            this.count = 1;
        }
    }
    login() {
        console.log("logni");
    }
    postDataToServer(data) {
        return this.http.post(this.baseURL + 'login', data);
    }
    register() {
    }
};
DashboardComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/dashboard/dashboard.component.scss")).default]
    })
], DashboardComponent);



/***/ }),

/***/ "./src/app/data-collecter/data-collecter.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/data-collecter/data-collecter.component.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wfull {\n  width: 100%;\n}\n\n.whalf {\n  width: 50%;\n}\n\n.wquarter {\n  width: 75%;\n}\n\n.wthird {\n  width: 24.4%;\n}\n\n.container {\n  display: -webkit-box;\n  display: flex;\n  flex-wrap: wrap;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: start;\n          justify-content: flex-start;\n}\n\n.leftpad8 {\n  padding-left: 8px;\n}\n\n.leftpad21 {\n  padding-left: 21px;\n}\n\n.z-depth-2 {\n  box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);\n  height: 25px;\n}\n\n.mat-divider {\n  border-top-width: 2rem;\n  border-top-color: transparent;\n}\n\n.mat-form-field {\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGF0YS1jb2xsZWN0ZXIvRDpcXF9VQkJcXExpY2Vuc3pcXF9fX19BbGxhbXZpenNnYSBnaXRcXGFsbGFtdml6c2dhLWJuYW0wMTU0LXViYi0yMDIwXFxrZXluYW1pY3Mvc3JjXFxhcHBcXGRhdGEtY29sbGVjdGVyXFxkYXRhLWNvbGxlY3Rlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZGF0YS1jb2xsZWN0ZXIvZGF0YS1jb2xsZWN0ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0FDQ0o7O0FERUE7RUFDSSxZQUFBO0FDQ0o7O0FERUE7RUFDSSxvQkFBQTtFQUFBLGFBQUE7RUFDQSxlQUFBO0VBQ0EsNEJBQUE7RUFBQSw2QkFBQTtVQUFBLHNCQUFBO0VBQ0EsdUJBQUE7VUFBQSwyQkFBQTtBQ0NKOztBREVBO0VBQ0ksaUJBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxnSEFBQTtFQUNBLFlBQUE7QUNDSjs7QURFQTtFQUNJLHNCQUFBO0VBQ0EsNkJBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9kYXRhLWNvbGxlY3Rlci9kYXRhLWNvbGxlY3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53ZnVsbCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLndoYWxmIHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbn1cclxuXHJcbi53cXVhcnRlciB7XHJcbiAgICB3aWR0aDogNzUlO1xyXG59XHJcblxyXG4ud3RoaXJkIHtcclxuICAgIHdpZHRoOiAyNC40JTtcclxufVxyXG5cclxuLmNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxufVxyXG5cclxuLmxlZnRwYWQ4IHtcclxuICAgIHBhZGRpbmctbGVmdDogOHB4O1xyXG59XHJcblxyXG4ubGVmdHBhZDIxIHtcclxuICAgIHBhZGRpbmctbGVmdDogMjFweDtcclxufVxyXG5cclxuLnotZGVwdGgtMiB7XHJcbiAgICBib3gtc2hhZG93OiAwIDRweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDFweCAxMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKSwgMCAycHggNHB4IC0xcHggcmdiYSgwLCAwLCAwLCAwLjMpO1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG59XHJcblxyXG4ubWF0LWRpdmlkZXIge1xyXG4gICAgYm9yZGVyLXRvcC13aWR0aDogMnJlbTtcclxuICAgIGJvcmRlci10b3AtY29sb3I6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn0iLCIud2Z1bGwge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLndoYWxmIHtcbiAgd2lkdGg6IDUwJTtcbn1cblxuLndxdWFydGVyIHtcbiAgd2lkdGg6IDc1JTtcbn1cblxuLnd0aGlyZCB7XG4gIHdpZHRoOiAyNC40JTtcbn1cblxuLmNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xufVxuXG4ubGVmdHBhZDgge1xuICBwYWRkaW5nLWxlZnQ6IDhweDtcbn1cblxuLmxlZnRwYWQyMSB7XG4gIHBhZGRpbmctbGVmdDogMjFweDtcbn1cblxuLnotZGVwdGgtMiB7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgMXB4IDEwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpLCAwIDJweCA0cHggLTFweCByZ2JhKDAsIDAsIDAsIDAuMyk7XG4gIGhlaWdodDogMjVweDtcbn1cblxuLm1hdC1kaXZpZGVyIHtcbiAgYm9yZGVyLXRvcC13aWR0aDogMnJlbTtcbiAgYm9yZGVyLXRvcC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/data-collecter/data-collecter.component.ts":
/*!************************************************************!*\
  !*** ./src/app/data-collecter/data-collecter.component.ts ***!
  \************************************************************/
/*! exports provided: DataCollecterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataCollecterComponent", function() { return DataCollecterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let DataCollecterComponent = class DataCollecterComponent {
    constructor(formBuilder, http) {
        this.formBuilder = formBuilder;
        this.http = http;
        this.cors = "https://cors-anywhere.herokuapp.com/";
        this.baseURL = this.cors + "https://keynamics-core.herokuapp.com/";
        this.chartimeVector = [];
        this.allTimeVector = [];
        this.startTimestamp = 0;
        this.stopTimestamp = 0;
        this.sentence = "indul a pap aludni";
        this.howMany = 10;
        this.iteration = 0;
        this.keydowntime = 0;
        this.keyuptime = 0;
        this.keypresstimevector = [];
        this.allKeypresstimevector = [];
        this.value = '';
        this.allowedWordsInOrder = ['i', 'in', 'ind', 'indu', 'indul', 'indul ', 'indul a', 'indul a ',
            'indul a p', 'indul a pa', 'indul a pap', 'indul a pap ', 'indul a pap a', 'indul a pap al',
            'indul a pap alu', 'indul a pap alud', 'indul a pap aludn', 'indul a pap aludni'];
        this.formCollecting = this.formBuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            age: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            gender: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            word: [''],
            numberofcoll: [this.howMany, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            typeword: [''],
            count: [this.howMany, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    ngOnInit() {
    }
    resetTypeWord() {
        this.formCollecting.patchValue({ typeword: '' });
        this.formCollecting.updateValueAndValidity();
    }
    changedNumberOfColl(value) {
        this.formCollecting.patchValue({ count: value });
    }
    nextStepper(stepper) {
        stepper.next();
    }
    save() {
        // this.counterIsNull = false;
        if (this.allTimeVector.length) {
            let date = new Date();
            // nev_nap_honap_ora_perc.txt
            var fileName = this.formCollecting.value.name + "_" + date.getDate() + "_" + date.getMonth() + "_" + date.getHours() + "_" + date.getMinutes() + ".txt";
            let blob = new Blob([JSON.stringify(this.formCollecting.value), '\n', JSON.stringify(this.allTimeVector), '\n', JSON.stringify(this.allKeypresstimevector)], { type: 'text/plain' });
            // let link = document.createElement('a');
            // link.download = fileName;
            // link.href = URL.createObjectURL(blob);
            // link.click();
            this.postDataToServer(blob, fileName).subscribe(res => { console.log(res); });
        }
    }
    postDataToServer(data, fileName) {
        // return this.http.post(this.baseURL + fileName, data)
        return this.http.post(this.baseURL + 'keystrokesdata', data);
    }
    onKeydown() {
        this.keydowntime = Date.now();
    }
    onKeyup(event, stepper) {
        this.value += event.key;
        console.log(this.value);
        console.log(this.allowedWordsInOrder[this.value.length - 1]);
        if (this.value != this.allowedWordsInOrder[this.value.length - 1]) {
            this.value = '';
            this.chartimeVector = [];
            this.keypresstimevector = [];
            this.resetTypeWord();
            this.value = '';
        }
        else {
            this.keyuptime = Date.now();
            this.keypresstimevector.push([event.key, this.keyuptime - this.keydowntime]);
            if (this.value.length == 1) {
                // elso alkalommal
                this.startTimestamp = Date.now();
            }
            else {
                this.chartimeVector.push(Date.now() - this.startTimestamp);
                this.startTimestamp = Date.now();
            }
            if (this.value == 'indul a pap aludni') {
                this.formCollecting.patchValue({ count: this.formCollecting.value.count - 1 });
                this.allTimeVector.push(this.chartimeVector);
                this.allKeypresstimevector.push(this.keypresstimevector);
                this.chartimeVector = [];
                this.keypresstimevector = [];
                this.resetTypeWord();
                this.value = '';
            }
        }
        if (this.formCollecting.value.count == 0) {
            // this.counterIsNull = true;
            this.nextStepper(stepper);
        }
    }
};
DataCollecterComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
DataCollecterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-data-collecter',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./data-collecter.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/data-collecter/data-collecter.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./data-collecter.component.scss */ "./src/app/data-collecter/data-collecter.component.scss")).default]
    })
], DataCollecterComponent);



/***/ }),

/***/ "./src/app/mainuserpage/mainuserpage.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/mainuserpage/mainuserpage.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".z-depth-2 {\n  box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);\n  height: 25px;\n}\n\n.matcardBlock {\n  max-width: 400px;\n  margin: 2em auto;\n  text-align: center;\n  height: 25rem;\n}\n\n.mat-form-field {\n  display: block;\n}\n\n.leftpad21 {\n  padding-left: 21px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbnVzZXJwYWdlL0Q6XFxfVUJCXFxMaWNlbnN6XFxfX19fQWxsYW12aXpzZ2EgZ2l0XFxhbGxhbXZpenNnYS1ibmFtMDE1NC11YmItMjAyMFxca2V5bmFtaWNzL3NyY1xcYXBwXFxtYWludXNlcnBhZ2VcXG1haW51c2VycGFnZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbWFpbnVzZXJwYWdlL21haW51c2VycGFnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdIQUFBO0VBQ0EsWUFBQTtBQ0NKOztBREVBO0VBRUksZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtBQ0FKOztBREdBO0VBQ0ksY0FBQTtBQ0FKOztBREdBO0VBQ0ksa0JBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL21haW51c2VycGFnZS9tYWludXNlcnBhZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuei1kZXB0aC0yIHtcclxuICAgIGJveC1zaGFkb3c6IDAgNHB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgMXB4IDEwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpLCAwIDJweCA0cHggLTFweCByZ2JhKDAsIDAsIDAsIDAuMyk7XHJcbiAgICBoZWlnaHQ6IDI1cHg7XHJcbn1cclxuXHJcbi5tYXRjYXJkQmxvY2sge1xyXG4gICAgLy8gcGFkZGluZy10b3A6IDQ1cHg7XHJcbiAgICBtYXgtd2lkdGg6IDQwMHB4O1xyXG4gICAgbWFyZ2luOiAyZW0gYXV0bztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGhlaWdodDogMjVyZW07XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZCB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuLmxlZnRwYWQyMSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDIxcHg7XHJcbn1cclxuIiwiLnotZGVwdGgtMiB7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgMXB4IDEwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpLCAwIDJweCA0cHggLTFweCByZ2JhKDAsIDAsIDAsIDAuMyk7XG4gIGhlaWdodDogMjVweDtcbn1cblxuLm1hdGNhcmRCbG9jayB7XG4gIG1heC13aWR0aDogNDAwcHg7XG4gIG1hcmdpbjogMmVtIGF1dG87XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgaGVpZ2h0OiAyNXJlbTtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5sZWZ0cGFkMjEge1xuICBwYWRkaW5nLWxlZnQ6IDIxcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/mainuserpage/mainuserpage.component.ts":
/*!********************************************************!*\
  !*** ./src/app/mainuserpage/mainuserpage.component.ts ***!
  \********************************************************/
/*! exports provided: MainuserpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainuserpageComponent", function() { return MainuserpageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let MainuserpageComponent = class MainuserpageComponent {
    constructor(http, activatedRoute) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.cors = "https://cors-anywhere.herokuapp.com/";
        this.baseURL = this.cors + "https://keynamics-core.herokuapp.com/";
        this.user_id = '';
        this.userName = '';
        this.score = '';
        this.log_prob = '';
        this.prob = '';
        this.keystroke_data = null;
    }
    ngOnInit() {
        this.activatedRoute.queryParams.subscribe(params => {
            this.keystroke_data = new Blob([params.keystroke_data, '|', JSON.stringify(params.predicted_id)], { type: 'text/plain' });
            // this.keystroke_data = new Blob([params.keystroke_data, '|', JSON.stringify('9')], {type: 'text/plain'});
            this.user_id = params.predicted_id;
            this.userName = params.userName;
            this.score = params.score;
            this.log_prob = params.log_prob;
            this.prob = params.prob;
        });
    }
    postLoginDataToServer(data) {
        return this.http.post(this.baseURL + 'loginfeedback', data);
    }
    postUntrainedDataToServer(data) {
        return this.http.post(this.baseURL + 'addtountraineddata', data);
    }
    correctUser() {
        this.postLoginDataToServer([1, this.user_id]).subscribe(res => { });
        this.postUntrainedDataToServer(this.keystroke_data).subscribe(res => { });
    }
    inCorrectUser() {
        this.postLoginDataToServer([0, this.user_id]).subscribe(res => { });
    }
};
MainuserpageComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
MainuserpageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-mainuserpage',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./mainuserpage.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/mainuserpage/mainuserpage.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./mainuserpage.component.scss */ "./src/app/mainuserpage/mainuserpage.component.scss")).default]
    })
], MainuserpageComponent);



/***/ }),

/***/ "./src/app/shared/components/header/header.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/shared/components/header/header.component.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/shared/components/header/header.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/components/header/header.component.ts ***!
  \**************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HeaderComponent = class HeaderComponent {
    constructor() { }
    ngOnInit() {
    }
};
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/header/header.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./header.component.scss */ "./src/app/shared/components/header/header.component.scss")).default]
    })
], HeaderComponent);



/***/ }),

/***/ "./src/app/shared/components/layout/layout.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/shared/components/layout/layout.component.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL2xheW91dC9sYXlvdXQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/shared/components/layout/layout.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/components/layout/layout.component.ts ***!
  \**************************************************************/
/*! exports provided: LayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutComponent", function() { return LayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let LayoutComponent = class LayoutComponent {
    constructor() { }
    ngOnInit() {
    }
};
LayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-layout',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./layout.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/layout/layout.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./layout.component.scss */ "./src/app/shared/components/layout/layout.component.scss")).default]
    })
], LayoutComponent);



/***/ }),

/***/ "./src/app/shared/components/navigation/navigation.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/shared/components/navigation/navigation.component.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/shared/components/navigation/navigation.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shared/components/navigation/navigation.component.ts ***!
  \**********************************************************************/
/*! exports provided: NavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return NavigationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NavigationComponent = class NavigationComponent {
    constructor() { }
    ngOnInit() {
    }
};
NavigationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-navigation',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./navigation.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/navigation/navigation.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./navigation.component.scss */ "./src/app/shared/components/navigation/navigation.component.scss")).default]
    })
], NavigationComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\_UBB\Licensz\____Allamvizsga git\allamvizsga-bnam0154-ubb-2020\keynamics\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map